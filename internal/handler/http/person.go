package http

import (
	"github.com/AlmasOrazgaliev/pac/internal/domain/person"
	"github.com/AlmasOrazgaliev/pac/internal/service"
	"github.com/AlmasOrazgaliev/pac/pkg/server/status"
	"github.com/gin-gonic/gin"
	"github.com/go-chi/render"
)

type PersonHandler struct {
	Service *service.Service
}

func NewPersonHandler(s *service.Service) *PersonHandler {
	return &PersonHandler{Service: s}
}

func (h *PersonHandler) Routes(rg *gin.RouterGroup) {
	r := rg.Group("/persons")
	r.GET("/search", h.list)
	r.POST("/", h.add)

}

// List of persons from the database with filters
//
//	@Summary	List of persons from the database with filters
//	@Tags		persons
//	@Accept		json
//	@Produce	json
//	@Success	200				{array}		person.Response
//	@Failure	500				{object}	status.Response
//	@Router		/persons 	[get]
func (h *PersonHandler) list(c *gin.Context) {
	res, err := h.Service.ListProduct(c.Request.Context(), c.Request)
	if err != nil {
		render.JSON(c.Writer, c.Request, status.InternalServerError(err))
		return
	}

	render.JSON(c.Writer, c.Request, status.OK(res))
}

// Add a new person to the database
//
//	@Summary	Add a new person to the database
//	@Tags		persons
//	@Accept		json
//	@Produce	json
//	@Param		request	body		person.Request	true	"body param"
//	@Success	200		{object}	person.Response
//	@Failure	400		{object}	status.Response
//	@Failure	500		{object}	status.Response
//	@Router		/persons [post]
func (h *PersonHandler) add(c *gin.Context) {
	req := person.Request{}
	if err := render.Bind(c.Request, &req); err != nil {
		render.JSON(c.Writer, c.Request, status.BadRequest(err, req))
		return
	}

	res, err := h.Service.AddProduct(c.Request.Context(), req)
	if err != nil {
		render.JSON(c.Writer, c.Request, status.InternalServerError(err))
		return
	}

	render.JSON(c.Writer, c.Request, status.OK(res))
}
