package handler

import (
	"github.com/AlmasOrazgaliev/pac/docs"
	"github.com/AlmasOrazgaliev/pac/internal/config"
	"github.com/AlmasOrazgaliev/pac/internal/handler/http"
	"github.com/AlmasOrazgaliev/pac/internal/service"
	"github.com/AlmasOrazgaliev/pac/pkg/server/router"
	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"net/url"
)

type Dependencies struct {
	Service *service.Service
	Configs config.Config
}

// Configuration is an alias for a function that will take in a pointer to a Handler and modify it
type Configuration func(h *Handler) error

// Handler is an implementation of the Handler
type Handler struct {
	dependencies Dependencies
	HTTP         *gin.Engine
}

// New takes a variable amount of Configuration functions and returns a new Handler
// Each Configuration will be called in the order they are passed in
func New(d Dependencies, configs ...Configuration) (h *Handler, err error) {
	// Create the handler
	h = &Handler{
		dependencies: d,
	}

	// Apply all Configurations passed in
	for _, cfg := range configs {
		// Pass the service into the configuration function
		if err = cfg(h); err != nil {
			return
		}
	}

	return
}

// @title Gin Swagger Example API
// @version 1.0
// @description Testing Swagger APIs.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:80
// @BasePath /api/v1
// @schemes http

// WithHTTPHandler applies a http handler to the Handler
func WithHTTPHandler() Configuration {
	return func(h *Handler) (err error) {
		// Create the http handler, if we needed parameters, such as connection strings they could be inputted here
		h.HTTP = router.New()

		docs.SwaggerInfo.BasePath = "/api/v1"
		docs.SwaggerInfo.Host = h.dependencies.Configs.HTTP.Host
		docs.SwaggerInfo.Schemes = []string{h.dependencies.Configs.HTTP.Schema}
		docs.SwaggerInfo.Title = "Test Service"

		swaggerURL := url.URL{
			Scheme: h.dependencies.Configs.HTTP.Schema,
			Host:   h.dependencies.Configs.HTTP.Host,
			Path:   "swagger/doc.json",
		}

		url := ginSwagger.URL(swaggerURL.String())
		h.HTTP.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler, url))
		personHandler := http.NewPersonHandler(h.dependencies.Service)

		v1 := h.HTTP.Group("/api/v1")
		personHandler.Routes(v1)

		return
	}
}
