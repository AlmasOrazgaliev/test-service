package service

import (
	"context"
	"github.com/AlmasOrazgaliev/pac/internal/domain/person"
	"net/http"
)

func (s *Service) ListProduct(ctx context.Context, r *http.Request) (res []person.Response, err error) {
	data, err := s.PersonRepository.Select(ctx, r)
	if err != nil {
		return
	}
	res = person.ParseFromEntities(data)

	return
}

func (s *Service) AddProduct(ctx context.Context, req person.Request) (res person.Response, err error) {
	data := person.Entity{
		Name:    &req.Name,
		Surname: &req.Surname,
		Age:     &req.Age,
		Address: &req.Address,
	}

	data.ID, err = s.PersonRepository.Create(ctx, data)
	if err != nil {
		return
	}
	res = person.ParseFromEntity(data)

	return
}
