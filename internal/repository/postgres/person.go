package postgres

import (
	"context"
	"fmt"
	"github.com/AlmasOrazgaliev/pac/internal/domain/person"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"net/http"
	"strings"
)

type PersonRepository struct {
	db *gorm.DB
}

func NewPersonRepository(db *gorm.DB) *PersonRepository {
	return &PersonRepository{
		db: db,
	}
}

func (s *PersonRepository) Select(ctx context.Context, r *http.Request) (dest []person.Entity, err error) {
	filters, args := s.prepareFilters(r)
	sorts := s.prepareSorts(r)
	//fmt.Println(filters, args)
	//query := fmt.Sprintf(`SELECT products.id, category_id, barcode, products.name, measure, cost, producer_country, brand_name, description, image, is_weighted `+
	//	`FROM products
	//     INNER JOIN categories c
	//	 ON products.category_id = c.id WHERE %s`, strings.Join(filters, " "))
	//query += " 1=1"

	dest = make([]person.Entity, 0)
	fmt.Println(strings.Join(filters, " ")+" 1=1", args)
	err = s.db.Where(strings.Join(filters, " ")+" 1=1", args...).Order(sorts).Find(&dest).Error
	//err = s.db.SelectContext(ctx, &dest, query, args...)

	return
}

func (s *PersonRepository) prepareSorts(r *http.Request) (sorts string) {
	sorts = r.URL.Query().Get("sort")
	return
}

func (s *PersonRepository) prepareFilters(r *http.Request) (filters []string, args []interface{}) {
	ageGTEFilter := r.URL.Query().Get("age_gte")
	if ageGTEFilter != "" {
		args = append(args, ageGTEFilter)
		filters = append(filters, "age >= ? AND")
	}

	ageLTEFilter := r.URL.Query().Get("age_lte")
	if ageLTEFilter != "" {
		args = append(args, ageLTEFilter)
		filters = append(filters, "age <= ? AND")
	}

	idFilter := r.URL.Query().Get("id")
	if idFilter != "" {
		args = append(args, idFilter)
		filters = append(filters, "id = ? AND")
	}

	nameFilter := r.URL.Query().Get("name")
	if nameFilter != "" {
		args = append(args, nameFilter)
		filters = append(filters, "name LIKE ? AND")
	}

	surnameFilter := r.URL.Query().Get("surname")
	if surnameFilter != "" {
		args = append(args, surnameFilter)
		filters = append(filters, "surname LIKE ? AND")
	}

	addressFilter := r.URL.Query().Get("address")
	if addressFilter != "" {
		args = append(args, addressFilter)
		filters = append(filters, "address LIKE ? AND")
	}

	return
}

func (s *PersonRepository) Create(ctx context.Context, data person.Entity) (id uuid.UUID, err error) {
	res := s.db.Model(person.Entity{}).Create(&data)
	return data.ID, res.Error
}
