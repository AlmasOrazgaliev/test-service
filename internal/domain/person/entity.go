package person

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Entity struct {
	gorm.Model
	ID      uuid.UUID `gorm:"primaryKey;type:uuid;default:GEN_RANDOM_UUID()"`
	Name    *string
	Surname *string
	Age     *int
	Address *string
}
