package person

import (
	"context"
	"github.com/google/uuid"
	"net/http"
)

type Repository interface {
	Select(ctx context.Context, r *http.Request) (dest []Entity, err error)
	Create(ctx context.Context, data Entity) (id uuid.UUID, err error)
}
