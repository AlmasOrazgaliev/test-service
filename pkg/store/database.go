package store

import (
	_ "database/sql"
	"errors"
	"fmt"
	"github.com/AlmasOrazgaliev/pac/internal/domain/person"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"net/url"
	"strings"

	_ "github.com/lib/pq"
)

// postgres://username:password@localhost:5432/dbname?sslmode=disable

type Database struct {
	schema         string
	driverName     string
	dataSourceName string

	Client *gorm.DB
}

func (s *Database) Print() {
	fmt.Println("Schema: ", s.schema)
	fmt.Println("DriverName: ", s.driverName)
	fmt.Println("Data source: ", s.dataSourceName)
	fmt.Println("client: ", s.Client)

}

// NewDatabase established connection to a database instance using provided URI and auth credentials.
func NewDatabase(schema, dataSourceName string) (database *Database, err error) {
	database = &Database{
		schema:         schema,
		dataSourceName: dataSourceName,
	}

	if err = database.connection(); err != nil {
		return
	}
	//err = database.createSchema()
	database.Print()

	return
}

func (s *Database) Migrate() (err error) {
	err = s.Client.AutoMigrate(&person.Entity{})
	if err != nil {
		return
	}
	return nil
}

func (s *Database) connection() error {
	err := s.parseDSN()
	//s.Print()
	//fmt.Println("in connection", s.dataSourceName)
	if err != nil {
		return err
	}
	s.Client, err = gorm.Open(postgres.Open(s.dataSourceName), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{TablePrefix: "public."},
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *Database) parseDSN() (err error) {
	if !strings.Contains(s.dataSourceName, "://") {
		err = errors.New("sql: undefined data source name " + s.dataSourceName)
		return
	}
	s.driverName = strings.ToLower(strings.Split(s.dataSourceName, "://")[0])

	source, err := url.Parse(s.dataSourceName)
	if err != nil {
		return
	}
	sourceQuery := source.Query()

	if s.schema != "" {
		switch s.driverName {
		case "postgres":
			sourceQuery.Set("search_path", s.schema)
		}
	}

	source.RawQuery = sourceQuery.Encode()
	s.dataSourceName = source.String()

	return
}

//func (s *Database) createSchema() (err error) {
//	if s.schema == "" {
//		return
//	}
//
//	query := ""
//	switch s.driverName {
//	case "postgres":
//		query = fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", s.schema)
//	}
//
//	_, err = s.Client.Exec(query)
//
//	return
//}
